# Recopelas

El proyecto se encuentra en /recopelas-master/proyectos/recopelas-ws
### Instrucciones para la compilación:
 - Netbeans versión 8.2
 - Ejecutar las bases de datos: **peliculas_fixed** y **titulos_fixed**
 - Configurar credenciales de base de datos encontrados en el paquete **com.upc.dao**, archivo **Database.java**
 - Dar prioridad como servidor **Glassfish 4.1.1**
 - Click derecho al proyecto y seleccionar **Run**

**Probado con jdk1.8.0_181**